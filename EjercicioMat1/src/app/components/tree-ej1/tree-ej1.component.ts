import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { InterfaceI } from 'src/app/interfaz/interface.datos';
import { TreeService } from 'src/app/services/tree.service';


@Component({
  selector: 'app-tree-ej1',
  templateUrl: './tree-ej1.component.html',
  styleUrls: ['./tree-ej1.component.css']
})
export class TreeEj1Component implements OnInit {

  formulario!: FormGroup

  valor1: string = ''
  number!: number;
  Valuenivel: number = 0;

  dataSource = new MatTreeNestedDataSource<InterfaceI>()
  treeControl = new NestedTreeControl<InterfaceI>(node => node.children)


  constructor(private fb: FormBuilder,
              private _treeService: TreeService) {
    this.formulario = fb.group({
      niveles: [1, [Validators.min(1), Validators.max(100), Validators.required]],
      repeticiones: [1, [Validators.min(1), Validators.max(100), Validators.required]],
    valor1: ['', [Validators.required]],
    valor2: ['', [Validators.required]],
    valor3: ['', [Validators.required]],
    valor4: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
  }


  agregar() {
   
    this.formulario.valid? this.generarDatos(): ""
  }
  
  hasChild = (_: number, node: InterfaceI) => !!node.children && node.children.length > 0;

  generarDatos() {
    let {niveles, repeticiones, valor1, valor2, valor3, valor4} = this.formulario.value
    var values =[valor1, valor2, valor3, valor4]
    var result:InterfaceI[] = []
    var primeraVez = true

    

    while (niveles > 0) {
      let value = this.getValue(niveles,values)

        result = this.anidacion(result, repeticiones, value)
      
      niveles--
    }
   
    this.dataSource.data = result;
  }

  getValue(num: number, values: string[]): string {
    if (num > 4) {
      return this.getValue(num-4, values)
    } else {
      return values[num-1]
    }
  }

  anidacion(res:InterfaceI[], rep:number, value:string): InterfaceI[] {
    var listHelp: InterfaceI[] = []

    for (let i = 0; i < rep; i++) {
      let pattern: InterfaceI={
        name: value,
        children: res
      }
      listHelp.push(pattern)
    }
    return listHelp
  }

  guardar(): void{
    console.log('guardar');
    
  console.log(this.formulario.value);
  this.Valuenivel = this.formulario.value.Nvel
  
 this.valor1 = this.formulario.value.valor1

  }
}